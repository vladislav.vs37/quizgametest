﻿using System.Collections;
using UnityEngine;

namespace _QuizGame.Tools
{
    public interface ICoroutineHolder
    {
        Coroutine StartCoroutine(IEnumerator enumerator);
        void StopCoroutine(Coroutine coroutine);
    }
}