﻿using UnityEngine;
using Zenject;

namespace _QuizGame.Tools
{
    public class CoroutineHolderFactory : IFactory<ICoroutineHolder>
    {
        public ICoroutineHolder Create()
        {
            var gameObject = new GameObject("CoroutineHolder");
            return gameObject.AddComponent<CoroutineHolder>();
        }
    }
}