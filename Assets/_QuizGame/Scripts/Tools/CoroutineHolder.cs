﻿using System.Collections;
using UnityEngine;

namespace _QuizGame.Tools
{
    public class CoroutineHolder : MonoBehaviour, ICoroutineHolder
    {
        Coroutine ICoroutineHolder.StartCoroutine(IEnumerator enumerator)
        {
            return StartCoroutine(enumerator);
        }
        
        void ICoroutineHolder.StopCoroutine(Coroutine coroutine)
        {
            StopCoroutine(coroutine);
        }
    }
}