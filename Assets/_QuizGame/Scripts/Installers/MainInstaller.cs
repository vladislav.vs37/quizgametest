﻿using _QuizGame.Tools;
using UnityEngine;
using Zenject;

namespace _QuizGame.Installers
{
    //Binds some main things needed for project
    [CreateAssetMenu(fileName = "Main Installer", menuName = "QuizGame/Installers/MainInstaller", order = 0)]
    public class MainInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            ModelsInstaller.Install(Container);
            BindTools();
        }

        private void BindTools()
        {
            Container.Bind<ICoroutineHolder>().FromFactory<CoroutineHolderFactory>().AsSingle().NonLazy();
        }
    }
}