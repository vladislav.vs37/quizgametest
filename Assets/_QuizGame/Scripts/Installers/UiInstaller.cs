﻿using _QuizGame.UserInterface.Basics;
using _QuizGame.UserInterface.Presenters;
using UnityEngine;
using Zenject;

namespace _QuizGame.Installers
{
    //Bind UI elements, Views, Presenters, UI System
    [RequireComponent(typeof(IUiSystem))]
    public class UiInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            BindUiSystem();
            BindViews();
            BindPresenters();
        }

        private void BindUiSystem()
        {
            var uiSystem = GetComponent<IUiSystem>();
            Container.Bind<IUiSystem>().FromInstance(uiSystem).AsSingle();
        }

        private void BindViews()
        {
            IUiView[] views = GetComponentsInChildren<IUiView>();

            Container.BindInstances(views);
        }

        private void BindPresenters()
        {
            Container.Bind<MainScreenPresenter>().AsSingle();
            Container.Bind<GameplayScreenPresenter>().AsSingle();
            Container.Bind<FinishScreenPresenter>().AsSingle();
        }
    }
}