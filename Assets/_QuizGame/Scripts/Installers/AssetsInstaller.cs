﻿using _QuizGame.Providers;
using UnityEngine;
using Zenject;

namespace _QuizGame.Installers
{
    //Binds assets and other this that contains in project
    [CreateAssetMenu(fileName = "Assets Installer", menuName = "QuizGame/Installers/AssetsInstaller", order = 0)]
    public class AssetsInstaller : ScriptableObjectInstaller
    {
        [SerializeField] private QuizSO _geographyQuiz;

        public override void InstallBindings()
        {
            Container.Bind<IQuizDataProvider>().FromInstance(_geographyQuiz);
        }
    }
}