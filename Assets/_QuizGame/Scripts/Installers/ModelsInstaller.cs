﻿using _QuizGame.Models;
using Zenject;

namespace _QuizGame.Installers
{
    public class ModelsInstaller : Installer<ModelsInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<Quiz>().FromNew().AsSingle().NonLazy();
            Container.Bind<Player>().FromNew().AsSingle().NonLazy();
            Container.Bind<GameApp>().FromNew().AsSingle().NonLazy();
        }
    }
}