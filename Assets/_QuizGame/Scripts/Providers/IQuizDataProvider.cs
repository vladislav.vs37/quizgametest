﻿using _QuizGame.DTO;

namespace _QuizGame.Providers
{
    public interface IQuizDataProvider
    {
        public QuizDTO GetQuizDTO();
    }
}