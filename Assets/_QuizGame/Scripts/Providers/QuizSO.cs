﻿using System.Linq;
using _QuizGame.DTO;
using UnityEngine;

namespace _QuizGame.Providers
{
    [CreateAssetMenu(fileName = "New Quiz", menuName = "QuizGame/Quiz", order = 0)]
    public class QuizSO : ScriptableObject, IQuizDataProvider
    {
        [SerializeField] private int _mistakesLimit;
        [SerializeField] private bool _randomQuestionMode;
        [SerializeField] private QuestionSO[] _questions;
        
        public QuizDTO GetQuizDTO()
        {
            return new QuizDTO
            {
                MistakesLimit = _mistakesLimit,
                RandomQuestionMode = _randomQuestionMode,
                Questions = _questions.Select(q => q.GetQuestionDTO()).ToArray()
            };
        }
    }
}