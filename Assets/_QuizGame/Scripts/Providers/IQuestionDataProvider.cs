﻿using _QuizGame.DTO;

namespace _QuizGame.Providers
{
    public interface IQuestionDataProvider
    {
        QuestionDTO GetQuestionDTO();
    }
}