﻿using System.Linq;
using _QuizGame.DTO;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _QuizGame.Providers
{
    [CreateAssetMenu(fileName = "New Questions", menuName = "QuizGame/Question", order = 0)]
    public class QuestionSO : ScriptableObject, IQuestionDataProvider
    {
        [InfoBox("Only one answer must be correct!", InfoMessageType.Error, "IsAnswersNotValid")]
        [SerializeField] private QuestionDTO _questionDTO; 
        
        public QuestionDTO GetQuestionDTO()
        {
            return _questionDTO;
        }

        private bool IsAnswersNotValid()
        {
            return _questionDTO.Answers.Count(a => a.IsCorrect) != 1;
        }
    }
}