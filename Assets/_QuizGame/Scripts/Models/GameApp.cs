﻿using UnityEngine;

namespace _QuizGame.Models
{
    //Model of application
    public class GameApp
    {
        public void QuitGame()
        {
            Application.Quit();
        }
    }
}