﻿using System;
using UnityEngine;

namespace _QuizGame.Models
{
    //Model of player. May contain best score, money, age etc.
    public class Player
    {
        private const string BestScoreKey = "BestScore";

        public event Action OnBestScoreUpdated;

        public int BestScore
        {
            get => PlayerPrefs.GetInt(BestScoreKey, 0);
            set
            {
                PlayerPrefs.SetInt(BestScoreKey, value);
                OnBestScoreUpdated?.Invoke();
            }
        }
    }
}