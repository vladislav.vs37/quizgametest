﻿using System;
using System.Collections.Generic;
using System.Linq;
using _QuizGame.DTO;
using _QuizGame.Providers;
using Random = UnityEngine.Random;

namespace _QuizGame.Models
{
    //Quiz model
    public class Quiz
    {
        private readonly Player _player;
        public event Action OnFailed;
        public event Action OnFinished;
        public event Action OnNextQuestion;
        public event Action OnMadeMistake;
        
        public TimeSpan ElapsedTime => _finished ? _finishDate - _startDate : DateTime.UtcNow - _startDate;
        public int Score { get; private set; }
        public int Mistakes { get; private set; }
        public bool Failed { get; private set; }
        
        private readonly QuizDTO _dto;
        
        private DateTime _startDate;
        private List<QuestionDTO> _questionsList;
        private int _currentQuestionIndex;
        private bool _finished;
        private DateTime _finishDate;

        private QuestionDTO CurrentQuestion => _questionsList[_currentQuestionIndex];

        public Quiz(IQuizDataProvider quizDataProvider, Player player)
        {
            _player = player;
            _dto = quizDataProvider.GetQuizDTO();
        }

        //Start the quiz
        public void Start()
        {
            Reset();
            _startDate = DateTime.UtcNow;
            _questionsList = new List<QuestionDTO>(_dto.Questions);

            if (_dto.RandomQuestionMode)
                ShuffleQuestionsList();
        }

        public QuestionDTO GetCurrentQuestion()
        {
            return CurrentQuestion.Clone();
        }

        //Accept answer and check if it correct or incorrect
        public void AcceptAnswer(AnswerDTO answerDTO)
        {
            if (answerDTO.IsCorrect)
            {
                Score += CurrentQuestion.Points;
                GoNextQuestion();
            }
            else
            {
                Mistakes++;
                OnMadeMistake?.Invoke();
                
                if (Mistakes >= _dto.MistakesLimit)
                    FailQuiz();
                else
                    GoNextQuestion();
            }
        }

        //Switch to next question and notify 
        private void GoNextQuestion()
        {
            _currentQuestionIndex++;

            if (_currentQuestionIndex >= _questionsList.Count)
                FinishQuiz();
            else
                OnNextQuestion?.Invoke();
        }

        //Set failed and finished state, save finish date and notify
        private void FailQuiz()
        {
            Failed = true;
            _finished = true;
            _finishDate = DateTime.UtcNow;
            
            OnFailed?.Invoke();
        }

        //Set finished state, save finish date and notify
        private void FinishQuiz()
        {
            _finished = true;
            _finishDate = DateTime.UtcNow;

            if (_player.BestScore < Score)
                _player.BestScore = Score;
            
            OnFinished?.Invoke();
        }

        private void ShuffleQuestionsList()
        {
            _questionsList = _questionsList.OrderBy(_ => Random.value).ToList();
        }

        private void Reset()
        {
            _finished = false;
            Failed = false;
            Score = 0;
            Mistakes = 0;
            _currentQuestionIndex = 0;
        }
    }
}