﻿using System;

namespace _QuizGame.DTO
{
    [Serializable]
    public class AnswerDTO : BaseDTO<AnswerDTO>
    {
        public bool IsCorrect;
        public string Text;
        
        public override AnswerDTO Clone()
        {
            return new AnswerDTO
            {
                IsCorrect = this.IsCorrect,
                Text = this.Text
            };
        }
    }
}