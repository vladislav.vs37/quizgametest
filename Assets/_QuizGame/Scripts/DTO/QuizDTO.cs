﻿using System;

namespace _QuizGame.DTO
{
    [Serializable]
    public class QuizDTO : BaseDTO<QuizDTO>
    {
        public int MistakesLimit;
        public bool RandomQuestionMode;
        public QuestionDTO[] Questions;
        
        public override QuizDTO Clone()
        {
            return new QuizDTO
            {
                MistakesLimit = this.MistakesLimit,
                RandomQuestionMode = this.RandomQuestionMode,
                Questions = this.Questions
            };
        }
    }
}