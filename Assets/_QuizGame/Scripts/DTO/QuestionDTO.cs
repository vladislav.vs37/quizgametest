﻿using System;

namespace _QuizGame.DTO
{
    [Serializable]
    public class QuestionDTO : BaseDTO<QuestionDTO>
    {
        public int Points;
        public string Text;
        public AnswerDTO[] Answers;
        
        public override QuestionDTO Clone()
        {
            return new QuestionDTO
            {
                Points = this.Points,
                Text = this.Text,
                Answers = this.Answers
            };
        }
    }
}