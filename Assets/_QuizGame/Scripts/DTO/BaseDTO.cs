﻿namespace _QuizGame.DTO
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">Type of nested DTO for create method Clone() return type</typeparam>
    public abstract class BaseDTO<T>
    {
        /// <summary>
        /// Creates copy of current object.
        /// </summary>
        /// <returns></returns>
        public abstract T Clone();
    }
}