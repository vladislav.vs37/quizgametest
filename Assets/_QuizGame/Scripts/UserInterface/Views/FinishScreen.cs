﻿using _QuizGame.UserInterface.Basics;
using _QuizGame.UserInterface.Presenters;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _QuizGame.UserInterface.Views
{
    public class FinishScreen : BaseUiView<FinishScreenPresenter>
    {
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private TextMeshProUGUI _timeText;
        [SerializeField] private TextMeshProUGUI _completeText;
        [SerializeField] private Button _toMenuButton;

        private void OnEnable()
        {
            _toMenuButton.onClick.AddListener(Presenter.OnToMenuButtonClick);
        }

        private void OnDisable()
        {
            _toMenuButton.onClick.RemoveListener(Presenter.OnToMenuButtonClick);
        }

        public void SetScore(int score) => _scoreText.text = score.ToString();
        
        public void SetTimeText(string time) => _timeText.text = time;

        public void SetCompleteText(string text) => _completeText.text = text;

        public void GoToMainMenu()
        {
            UiSystem.Hide(this);
            UiSystem.Show<MainScreen>();
        }
    }
}