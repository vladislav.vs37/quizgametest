﻿using System.Collections.Generic;
using _QuizGame.DTO;
using _QuizGame.UserInterface.Basics;
using _QuizGame.UserInterface.Elements;
using _QuizGame.UserInterface.Presenters;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace _QuizGame.UserInterface.Views
{
    public class GameplayScreen : BaseUiView<GameplayScreenPresenter>
    {
        [SerializeField] private TextMeshProUGUI _timeText;
        [SerializeField] private TextMeshProUGUI _mistakesText;
        [SerializeField] private TextMeshProUGUI _questionText;
        [SerializeField] private RectTransform _optionsContainer;
        [SerializeField, AssetsOnly] private AnswerOption _answerOptionPrefab;

        private readonly List<AnswerOption> _answerOptions = new();

        public void SetTimeText(string time)
        {
            _timeText.text = time;
        }

        public void SetMistakesText(string mistakes)
        {
            _mistakesText.text = mistakes;
        }

        public void GoToFinishScreen()
        {
            UiSystem.Hide(this);
            UiSystem.Show<FinishScreen>();
        }

        public void UpdateQuestion(QuestionDTO questionDTO)
        {
            _questionText.text = questionDTO.Text;
            UpdateAnswerOptions(questionDTO.Answers.Length);
            
            for (var i = 0; i < questionDTO.Answers.Length; i++)
            {
                AnswerDTO answer = questionDTO.Answers[i];
                _answerOptions[i].gameObject.SetActive(true);
                _answerOptions[i].SetText(answer.Text);
                _answerOptions[i].SetOnChooseCallback(() => Presenter.OnAnswerClick(answer));
            }
        }

        /// <summary>
        /// Adds or hides answer options depends on count of answers needed.
        /// </summary>
        /// <param name="optionsCount">Answers count</param>
        private void UpdateAnswerOptions(int optionsCount)
        {
            if (optionsCount > _answerOptions.Count)
                AddAnswerOptions(optionsCount - _answerOptions.Count);
            else if (optionsCount < _answerOptions.Count)
                HideAnswerOptions(_answerOptions.Count - optionsCount);
        }

        private void HideAnswerOptions(int count)
        {
            for (int i = _answerOptions.Count - 1; i >= _answerOptions.Count - count; i--)
                _answerOptions[i].gameObject.SetActive(false);
        }

        private void AddAnswerOptions(int count)
        {
            for (var i = 0; i < count; i++)
                _answerOptions.Add(Instantiate(_answerOptionPrefab, _optionsContainer));
        }
    }
}