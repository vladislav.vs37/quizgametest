﻿using _QuizGame.UserInterface.Basics;
using _QuizGame.UserInterface.Presenters;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _QuizGame.UserInterface.Views
{
    public class MainScreen : BaseUiView<MainScreenPresenter>
    {
        [SerializeField] private Button _startPlayButton;
        [SerializeField] private Button _quitButton;
        [SerializeField] private TextMeshProUGUI _bestScoreText;

        private void OnEnable()
        {
            _startPlayButton.onClick.AddListener(Presenter.OnStartPlayClick);
            _quitButton.onClick.AddListener(Presenter.OnQuitClick);
        }

        private void OnDisable()
        {
            _startPlayButton.onClick.RemoveListener(Presenter.OnStartPlayClick);
            _quitButton.onClick.RemoveListener(Presenter.OnQuitClick);
        }

        public void SetBestScore(int bestScore)
        {
            _bestScoreText.text = bestScore.ToString();
        } 
        
        public void GoToGameplayPage()
        {
            UiSystem.Hide(this);
            UiSystem.Show<GameplayScreen>();
        }
    }
}