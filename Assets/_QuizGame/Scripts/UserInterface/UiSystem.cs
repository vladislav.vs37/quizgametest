﻿using System;
using System.Collections.Generic;
using System.Linq;
using _QuizGame.UserInterface.Basics;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _QuizGame.UserInterface
{
    public class UiSystem : MonoBehaviour, IUiSystem
    {
        [SerializeField, ValidateInput("IsUiView", "Is must implement IUiView Interface")] 
        private GameObject _startScreen;
        
        private readonly List<IUiView> _views = new();

        private void Awake()
        {
            _views.AddRange(GetComponentsInChildren<IUiView>());

            foreach (IUiView uiView in _views)
                uiView.Hide(false);
        }

        private void Start()
        {
            ShowStartPage();
        }

        public void Show<TView>() where TView : IUiView
        {
            IUiView view = GetView<TView>();
            view.Show();
            view.MoveTopOrder();
        }

        public void Hide(IUiView uiView)
        {
            uiView.Hide();
        }

        private IUiView GetView<TView>()
        {
            IUiView view = _views.FirstOrDefault(v => v is TView);

            if (view == null)
                throw new Exception($"View of type {typeof(TView)} not found!");

            return view;
        }

        private void ShowStartPage()
        {
            var view = _startScreen.GetComponent<IUiView>();

            if (view == null)
                throw new Exception("Start page must implement IUiView Interface");

            view.Show();
            view.MoveTopOrder();
        }

        private bool IsUiView()
        {
            var view = _startScreen.GetComponent<IUiView>();

            return view != null;
        }
    }
}