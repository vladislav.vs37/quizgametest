﻿namespace _QuizGame.UserInterface.Basics
{
    public abstract class BasePresenter<TView> : IUiPresenter where TView : IUiView
    {
        protected TView View;
        
        protected BasePresenter(TView view)
        {
            View = view;
        }

        public abstract void OnViewShown();
        public abstract void OnViewHidden();
    }
}