﻿namespace _QuizGame.UserInterface.Basics
{
    public interface IUiSystem
    {
        public void Show<TView>() where TView : IUiView;
        public void Hide(IUiView uiView);
    }
}