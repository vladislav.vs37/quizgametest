﻿using UnityEngine;
using Zenject;

namespace _QuizGame.UserInterface.Basics
{
    public abstract class BaseUiView<TPresenter> : MonoBehaviour, IUiView where TPresenter : IUiPresenter
    {
        protected TPresenter Presenter;
        protected IUiSystem UiSystem;

        [Inject]
        public void Construct(TPresenter presenter, IUiSystem uiSystem)
        {
            Presenter = presenter;
            UiSystem = uiSystem;
        }

        public virtual void Show(bool notify = true)
        {
            gameObject.SetActive(true);
            
            if(notify)
                Presenter.OnViewShown();
        }

        public virtual void Hide(bool notify = true)
        {
            gameObject.SetActive(false);
            
            if(notify)
                Presenter.OnViewHidden();
        }

        void IUiView.MoveTopOrder()
        {
            transform.SetAsLastSibling();
        }
    }
}