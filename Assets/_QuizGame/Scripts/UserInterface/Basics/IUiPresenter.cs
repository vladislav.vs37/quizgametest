﻿namespace _QuizGame.UserInterface.Basics
{
    public interface IUiPresenter
    {
        public void OnViewShown();
        public void OnViewHidden();
    }
}