﻿namespace _QuizGame.UserInterface.Basics
{
    public interface IUiView
    {
        public void Show(bool notify = true);
        public void Hide(bool notify = true);

        public void MoveTopOrder();
    }
}