﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _QuizGame.UserInterface.Elements
{
    [RequireComponent(typeof(Button))]
    public class AnswerOption : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _answerText;

        private Action _onChoose;
        private Button _button;

        private void Awake()
        {
            _button = GetComponent<Button>();
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(OnButtonClick);
        }
        
        private void OnDisable()
        {
            _button.onClick.RemoveListener(OnButtonClick);
        }

        public void SetText(string text)
        {
            _answerText.text = text;
        }

        public void SetOnChooseCallback(Action onChoose)
        {
            _onChoose = onChoose;
        }

        private void OnButtonClick()
        {
            _onChoose?.Invoke();
        }
    }
}