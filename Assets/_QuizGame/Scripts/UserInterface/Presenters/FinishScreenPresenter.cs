﻿using _QuizGame.Models;
using _QuizGame.UserInterface.Basics;
using _QuizGame.UserInterface.Views;

namespace _QuizGame.UserInterface.Presenters
{
    public class FinishScreenPresenter : BasePresenter<FinishScreen>
    {
        private const string WinColor = "#31C357";
        private const string FailColor = "#C33B31";
        
        private readonly Quiz _quiz;
        
        private string FailedText => $"<color={FailColor}>Test failed! Try again!";
        private string CompleteText => $"<color={WinColor}>Test completed!";

        public FinishScreenPresenter(FinishScreen view, Quiz quiz) : base(view)
        {
            _quiz = quiz;
        }

        public override void OnViewShown()
        {
            View.SetScore(_quiz.Score);
            View.SetTimeText(_quiz.ElapsedTime.ToString(@"mm\:ss"));
            View.SetCompleteText(_quiz.Failed ? FailedText : CompleteText);
        }

        public override void OnViewHidden()
        {
        }

        public void OnToMenuButtonClick() => View.GoToMainMenu();
    }
}