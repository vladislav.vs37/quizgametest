﻿using _QuizGame.Models;
using _QuizGame.UserInterface.Basics;
using _QuizGame.UserInterface.Views;

namespace _QuizGame.UserInterface.Presenters
{
    public class MainScreenPresenter : BasePresenter<MainScreen>
    {
        private readonly GameApp _gameApp;
        private readonly Player _player;

        public MainScreenPresenter(MainScreen view, GameApp gameApp, Player player) : base(view)
        {
            _gameApp = gameApp;
            _player = player;
        }

        public override void OnViewShown()
        {
            View.SetBestScore(_player.BestScore);
        }

        public override void OnViewHidden()
        {
        }

        public void OnStartPlayClick()
        {
            View.GoToGameplayPage();
        }

        public void OnQuitClick()
        {
            _gameApp.QuitGame();
        }
    }
}