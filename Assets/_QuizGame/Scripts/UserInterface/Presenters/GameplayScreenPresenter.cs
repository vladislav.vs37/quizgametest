﻿using System.Collections;
using _QuizGame.DTO;
using _QuizGame.Models;
using _QuizGame.Tools;
using _QuizGame.UserInterface.Basics;
using _QuizGame.UserInterface.Views;
using UnityEngine;

namespace _QuizGame.UserInterface.Presenters
{
    public class GameplayScreenPresenter : BasePresenter<GameplayScreen>
    {
        private const string MistakesPrefix = "mistakes: ";
        private const float TimeUpdateStepInSeconds = .5f;
        
        private readonly Quiz _quiz;
        private readonly ICoroutineHolder _coroutineHolder;
        private Coroutine _timeUpdateCoroutine;

        public GameplayScreenPresenter(GameplayScreen view, Quiz quiz, ICoroutineHolder coroutineHolder) : base(view)
        {
            _quiz = quiz;
            _coroutineHolder = coroutineHolder;
        }

        public override void OnViewShown()
        {
            _quiz.OnFailed += OnQuizFailed;
            _quiz.OnFinished += OnQuizFinished;
            _quiz.OnMadeMistake += OnUserMadeMistake;
            _quiz.OnNextQuestion += OnNextQuestion;
            
            _quiz.Start();
            View.SetMistakesText(MistakesPrefix + 0);
            View.UpdateQuestion(_quiz.GetCurrentQuestion());
            StartTimeUpdateCoroutine();
        }

        public override void OnViewHidden()
        {
            _quiz.OnFailed -= OnQuizFailed;
            _quiz.OnFinished -= OnQuizFinished;
            _quiz.OnMadeMistake -= OnUserMadeMistake;
            _quiz.OnNextQuestion -= OnNextQuestion;
            
            StopTimeUpdateCoroutine();
        }

        public void OnAnswerClick(AnswerDTO answer)
        {
            _quiz.AcceptAnswer(answer);
        }

        private void OnNextQuestion()
        {
            View.UpdateQuestion(_quiz.GetCurrentQuestion());
        }

        private void OnUserMadeMistake()
        {
            View.SetMistakesText(MistakesPrefix + _quiz.Mistakes);
        }

        private void OnQuizFinished()
        {
            View.GoToFinishScreen();
        }

        private void OnQuizFailed()
        {
            View.GoToFinishScreen();
        }

        private void StartTimeUpdateCoroutine()
        {
            _timeUpdateCoroutine = _coroutineHolder.StartCoroutine(TimeUpdateCoroutine());
        }

        private void StopTimeUpdateCoroutine()
        {
            _coroutineHolder.StopCoroutine(_timeUpdateCoroutine);
        }

        private IEnumerator TimeUpdateCoroutine()
        {
            var waiter = new WaitForSeconds(TimeUpdateStepInSeconds);
            
            while (true)
            {
                View.SetTimeText(_quiz.ElapsedTime.ToString(@"mm\:ss"));
                
                yield return waiter;
            }
        }
    }
}